---
title: Proyectos personales
subtitle: Tecnologías y aficiones
comments: true
---

En este apartado hago referencia a mis aficiones, proyectos en el plano personal, talleres de formación y charlas que suelo organizar en mi tiempo libre.

# Blogs personales

En [este blog](https://soka.gitlab.io/blog) público artículos sobre nuevas tecnologías y programación.

Este [otro blog en Wordpress](https://ikerlandajuela.wordpress.com/) también contiene artículos mios.

# Taller gestión de proyectos colaborativos y productividad

[Contenidos del taller](https://soka.gitlab.io/TallerTrello/) dirigido a pequeñas empresas y asociaciones que buscan trabajar de forma colaborativa y aumentar su productividad con herramientas digitales.

# Angular: Framwork TypeScript desarrollo Web

[Web](https://soka.gitlab.io/angular/) dedicada al _framework_ de programación [Angular](https://angular.io/) de Google para el desarrollo de aplicativos Web ([enlace](https://soka.gitlab.io/angular/) del sitio).

# Scripting PowerShell

[Sitio Web](https://soka.gitlab.io/PSFabrik/) dedicado a scripting en PowerShell para entornos Windows y dominios Active Directory.

# Programación C Sharp

[Sitio Web](https://soka.gitlab.io/csharp/) dedicado a la programación en C#.

# Lenguaje R

[Repositorio en GitLab](https://gitlab.com/soka/r) con artículos básicos dedicados al lenguaje [R](https://www.r-project.org/) y al análisis estadístico y generación de digaramas.

# Taller radio libre en Internet

Taller impartido sobre como montar una radio libre en Internet con Libretime, contenidos en este [enlace](https://soka.gitlab.io/RadioLibre/).

# Charla soberanía tecnológica

Algunos [contenidos](https://ikerlandajuela.github.io/charla-soberania-tecnologica/) de la charla.
