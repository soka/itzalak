---
title: Carrera profesional
subtitle: Experiencia, tecnologías y proyectos profesionales
comments: true
toc: true
---

## Responsable área TI - Fundación Alboan y Fundación Arrupe Etxea (2019 - Actualidad)

Responsable TI de varías fundaciones del sector social: [Fundación Alboan](https://www.alboan.org/es), [Fundación Social Ignacio Ellacuria](http://fundacionellacuria.org/) y [Fundación Arrupe Etxea](https://arrupeetxea.org/).

A cargo de todas las infraestructuras de comunicaciones y datos de las sedes, funciones generales:

- Búsqueda de nuevas tecnologías, negociación con proveedores y compras.
- Consultoría TIC.
- Acompañamiento transformación digital y renovación TIC.

## Responsable área TI - KPR Consulting (2014 - 2019)

Inicialmente como **analista programador y responsable de proyectos** el primer año y medio aproximadamente, a continuación me proponen para el cargo de **responsable del área TI y del conjunto de proyectos informáticos de RSK (Red Social Koopera)**.

Entre las funciones que desempeño destaca la **coordinación** del equipo de informática, formado por ocho personas repartidas entre el **área de sistemas y soporte general** y el **área de proyectos** ubicadas en diferentes sedes y países.  

**Funciones generales**:

- Búsqueda de nuevas tecnologías, proveedores y socios tecnológicos.
- Implantación de metodologías y herramientas de gestión de proyectos, equipos y desarrollo software.
- Estudio y optimización de procesos, flujos de trabajo y sistemas de información existentes.

A continuación describo algunos de los proyectos más destacados en los que he trabajado o colaborado.

### Línea de clasificación

Aplicación de clasificación de artículos semi automatizada, basado en un **sistema de reconocimiento de voz** e interfaz con pantalla táctil para operarios, aplicación de control central de todos los puestos y servidores con cuadro de mando para la supervisión global.

Programación a bajo nivel sistema de detección presencia mediante **fotocélulas, control movimiento de cintas y clasificación en salida mediante aire neumático**. Control de tarjetas entradas / salidas basado en protocolo industrial de de comunicaciones [CAN](https://es.wikipedia.org/wiki/Bus_CAN).

Desarrollo de **autómatas multitarea y protocolo de comunicaciones en red con PLCs OMRON** para control de procesos logísticos que alimentan la línea.

**Tecnologías**: Aplicaciones en lenguajes C y C#. Equipos industriales.

### Sistema de clasificación basado en la composición material

Basado en la detección de la composición del artículo usando un espectrógrafo de infrarrojo cercano NIR [sIRoSort](http://www.gut-stuttgart.de/en/products/sirosort-stationary-nir-plastic-sorting-plant-for-bigger-parts-from-gut-environmental-technologies.html).

**Tecnologías y desarrollos**:

- Aplicación en lenguaje C para intervención del operario y comunicaciones RS232 con la máquina de reconocimiento, interacción con base de datos MySQL ([libmysqlclient](https://dev.mysql.com/downloads/connector/c/)), comunicaciones en red con _sockets_, _multithreading_ y configurable vía ficheros JSON.
- Desarrollo frontend administración de datos en PHP, MySQL y [Bootstrap](https://getbootstrap.com/).

### Sistema de detección de llenado de contenedores

En colaboración con [Bizintek](https://bizintek.es/) y EASI Technologies and Consulting Services. Desarrollo de un sistema de detección del nivel de llenado de contenedores mediante sensores y dispositivos IoT/M2M FlyportPRO [openPicus](https://en.wikipedia.org/wiki/OpenPicus) con comunicaciones GPRS basado en lenguaje C.

![](https://soka.gitlab.io/blog/post/2018-10-1-imagenes-sin-mas/images/openpicus-flyport-01.jpeg)

Framework PHP [Yii](https://www.yiiframework.com/) para la recepción y gestión de los datos de la red de sensores.

### Sistema de alquiler de bicicletas públicas

Integración de [pasarela de pago](https://www.kprprojects.org/bizimetaweb/) y gestión de usuarios para sistema movilidad urbana basado en [Stripe](https://stripe.com/es), [Bootstrap](https://getbootstrap.com/) y [MySQL](https://www.mysql.com/).

![](https://soka.gitlab.io/blog/post/2018-10-1-imagenes-sin-mas/images/bizimeta-web-01.png)

[**Web informativa**](http://bizimeta.com/) servicio movilidad urbana basada en [WordPress](https://wordpress.org/).

### Proyectos eCommerce en PrestaShop

[**Dos comercios en línea**](https://kooperashoponline.org/santiago/) al por mayor en Chile, basado en [PrestaShop](https://www.prestashop.com/es) y conector PHP para consolidación de datos bidireccional con aplicación comercial de gestión de stock y ventas.

![](https://soka.gitlab.io/blog/post/2018-10-1-imagenes-sin-mas/images/ecommerce-koopera-chile-01.png)

**Comercio en línea** basado en [PrestaShop](https://www.prestashop.com/es) para la venta de electrodomésticos de segunda mano (actualmente proyecto discontinuado).

![](https://soka.gitlab.io/blog/post/2018-10-1-imagenes-sin-mas/images/Koopera-electro-content-01.jpg)

---

## Analista programador - TELVENT tráfico y transporte (2006 – 2013)

Desarrollo y adaptación de sistemas CAE (Control de Acceso a la Estación) para diferentes proyectos en el área de proyectos ticketing.

**Funciones**:

- Desarrollo **aplicaciones tratamiento de títulos de transporte** de viajeros para sistemas de tren y metro (soportes en banda magnética y tarjetas sin contacto).
- Desarrollo aplicaciones en lenguaje C para sistemas operativos Linux (Fedora Core y sistemas embebidos Elinosh y ucLinux), Windows XP y Windows CE: Multihilo, Sockets, captura de señales, libxml, tratamiento E/S digitales, administración del sistema operativo, conocimientos básicos módulos del kernel, etc…
- Desarrollo librerías dinámicas y estáticas multiplataforma.
- Puestas en marcha, configuración y testeo de sistemas CAE.
- Tratamiento de archivos de configuracion XML (eXtensible Markup Language) mediante consultas XPath (XML Path Language) (basado en [Libxml2 de Gnome](http://www.xmlsoft.org/)), ejecución desde C de Javascript con [SpiderMonkey](https://developer.mozilla.org/en-US/docs/SpiderMonkey).
- Programación y diseño autómatas.
- Desarrollo protocolos de mensajería de monitorización, alarmas, tránsitos de viajeros con servidor central que permiten un control total del funcionamiento del sistema de CAE y tratamiento de títulos.
- Sistemas de ticketing basados en TSC (Tarjetas sin contacto) y bandas magnéticas ISO.
- Desarrollo API comunicaciones con aplicaciones Java a través de capa JNI (Java Native Interface), librerías de enlace dinámico,
- Diferentes arquitecturas físicas de sistemas embebidos basados en S.O: Elinosh, ucLinux, Windows CE con SDK.

**Tecnologías:**

- Fedora Core 6 y Windows XP como plataformas de desarrollo y pruebas. S.O embebidos Elinosh, ucLinux, WinCE.
- Herramientas de desarrollo Visual C++, EVC++ (Embedded Visual C++). compiladores (Gnu/GCC, DJGPP, Borland), programación scripts Shell Linux y Batch, GDB.

![](https://ikerlandajuela.files.wordpress.com/2013/06/n1520801525_195284_3224.jpg?w=240&h=180)

**Proyectos:**

- Sistema validación títulos de banda magnética para Sistema de Tren de Cercanías.
- Instalación y puesta en marcha de Sistema tren Caracas (Venezuela) basado en TSC (Tarjetas Sin Contacto) y desarrollo del sistema de validación títulos.
- Presentación oferta para metro de El Cairo (Egipto): Desarrollando labores de técnico del sistema CAE, adaptaciones del SW al HW del cliente. Instalación, puesta en marcha y testeo. Ofrecer asistencia durante presentación de la oferta. Colaboración en la simulación de una estación de metro prácticamente funcional.
- Integración de nuestro SW CAE con tecnologías existentes de otras empresas para empresa ferroviaria: Integración de CAE con ordenadores centrales de estación pertenecientes a empresa de la competencia: adaptación a nuevos protocolos de mensajería, sistema de configuración.
- Desarrollo sistema integral control acceso a la estación para Metro Valencia (Venezuela).

---

## Gerente Proyectos - INKOA ingeniería agroalimentaria (2003 – 2005)

En el departamento I+D responsable de todas las etapas de desarrollo del software y hardware en proyectos.

**Funciones:**

- **Gestión integral del proyecto** (asistencia a clientes, gestión documentación y defensas proyectos, selección de soluciones tecnológicas y evaluación de alternativas óptimas como proveedor plataformas embebidas, herramientas desarrollo, etc…). Diseño modelos relacionales de BBDD (Bases de Datos), etc…
- **Programación de aplicaciones embebidas** (Windows CE) basadas en plataforma embebida MipScale con S.O WindowsCE y aplicaciones Windows en VC++ (Visual C++).
- **Desarrollo de aplicaciones** con tecnologías de acceso a Internet (GPRS, GSM, Satélite) de dispositivos embebidos (programación de sockets, pequeños autómatas, orientación cliente servidor). Protocolos de comunicación con otros periféricos (antenas RFID de identificación electrónica animal, módems satelitales o GPRS mediante comandos Hayes).
- **Programación sitios web** dinámicos altamente configurables por el cliente con backend de administración de clientes orientados a gestión de pequeñas y medianas empresas (WAMP = Windows + Apache + MySQL + PHP).
- **Integración y programación de sensórica digital y analógica**: Sensores climatológicos de radiación solar, fotocélulas detección de presencia, electro válvulas neumáticas, paneles solares, etc.

**Tecnologías:**

- Herramientas y Lenguajes de Desarrollo: EVC++ 4.0, Visual C++ 6.0, PHP 5.0, MySQL 4.0, XML, Apache 2.0, JavaScript, SQL, Sockets, programación módem mediante comandos AT para acceder a Internet.
- Plataforma Embebida [MipSCALE](http://www.mipsasoft.com/MIPSCALE/hardware/mipscale.htm) con S.O WinCE.

**Proyectos:**

![](https://ikerlandajuela.files.wordpress.com/2013/06/inkoa_01.jpg?w=199&h=300)

- **EXPERTIC “Sistema experto para el diagnóstico y predicción de riesgos en la agricultura”**: Desarrollo de un sistema experto, que basado en TIC’s, permita el diagnóstico y la predicción de riesgos de enfermedades en el sector agrícola, mediante la incorporación de sistemas de captación automática de variables climatológicas (estación meteorológica), modelos predictivos y comunicaciones remotas (GPRS, GSM-SMS, Satélite). Creación y programación de una estación metereológica: Desarrollo cuadro eléctrico con sistema embebido MipSCALE compuesto por entradas / salidas digitales y analógicas conectadas a sensores metereológicos que permiten captar variables climatológicas y enviarlas vía GPRS / Satélite a un servidor MySQL en Internet. Aplicaciones para explotar los datos del servidor así como un sistema de envio de alarmas mediante SMS.
- **SELEKTA “Manga de clasificación y selección automática basada en identificación electrónica animal”**: Sistema de identificación electrónica animal basado en chips RFID (radiofrecuencia) que permite la separación individualizada (fotocélulas de presencia, salidas / puertas comandadas mediante electro-válvulas neumáticas) basada en criterios configurables. Desarrollado bajo la plataforma MipSCALE en WinCE comunicado con una antena RFID capaz de leer los ID (Identificadores) electrónicos animales, permite sin intervención humana clasificar animales basado en tres salidas automatizadas. Aplicación gráfica basada en pantalla táctil para funciones auxiliares de gestión.
- **LEADER+ “Implementación de un sistema de trazabilidad en viña basado en tecnologías de la información”**: Diseño y desarrollo de un sistema Web que permita la gestión / trazabilidad integral de todo los procesos implicados en una viña (bodegas, parcelas…)

## Otros trabajos

En ocasiones y de forma puntual durante los últimos años he desarrollado varios sitios Web para pequeñas empresas, sitios dinámicos en lenguaje PHP y con sistema de gestión de base de datos MySQL, con backend para que el cliente pueda administrar la web por su propia cuenta,multilenguaje, catálogos de productos con tienda online con PayPal.

Algunos ejemplos:

Refratermic:

![](https://ikerlandajuela.files.wordpress.com/2013/06/refra_01.png?w=300&h=206)

Administraciones Orejón:

![](https://ikerlandajuela.files.wordpress.com/2013/06/orejon_1.png?w=300&h=174)

IMT Ingeniería de Moldes y troqueles

![](https://ikerlandajuela.files.wordpress.com/2013/06/imtsl.png?w=300)

EconElectric Norte, Productos de eficiencia energética

![](https://ikerlandajuela.files.wordpress.com/2013/07/econelectric.png?w=231&h=300)
