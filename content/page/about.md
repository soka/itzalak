---
title: About me
subtitle: Why you'd want to hang out with me
comments: true
---

- Carrera profesional
- Proyectos personales y aficiones

# Contacto

- Correos: ikernaix@gmail.com, ikerlandajuela@gmail.com
- Twitter [@ikernaix](https://twitter.com/ikernaix).
- GitLab [Soka](https://gitlab.com/soka): [Blog](https://soka.gitlab.io/blog/), programación [Electron](https://soka.gitlab.io/electron/), taller, productividad y gestión proyectos con [Trello](https://soka.gitlab.io/TallerTrello/), [Powershell](https://soka.gitlab.io/PSFabrik/), monitorización de equipos en red con [Zabbix](https://soka.gitlab.io/ZabbixMan/), programación [R](https://gitlab.com/soka/r), taller radio libre (https://soka.gitlab.io/RadioLibre/), ...
- Mastodon [@popu@mastodon.social](https://mastodon.social/@popu).
- [Blog en WP](http://ikerlandajuela.wordpress.com) un poco parado.. [Formación](https://ikerlandajuela.wordpress.com/formacion/), [Trabajos](https://ikerlandajuela.wordpress.com/experiencia-laboral/), Scripting MS Win [Powershell](https://ikerlandajuela.wordpress.com/category/programacion/powershell/), [C#](https://ikerlandajuela.wordpress.com/category/programacion/csharp/), [Arduino](https://ikerlandajuela.wordpress.com/category/arduino/), ...
- [Linkedin](https://www.linkedin.com/in/iker-landajuela/) (no me gusta mucho).
