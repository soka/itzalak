---
title: Formación
subtitle: Cursos, talleres y charlas
comments: true
---

## Datos Académicos

- (1997-2001) Ingeniero Técnico en Informática de Sistemas – Universidad de Mondragón.
- (2002-2003) Ingeniero Informático – Universidad de Mondragón.

## Idiomas

- Idiomas maternos: castellano y euskera.
- Euskera: hablado, escrito y técnico. Titulo EGA (1997).
- Inglés: hablado y escrito (nivel medio). Compresión perfecta de documentos técnicos. Título B1.

## Formación Complementaria

- (2021) ["Cómo hacer e-mailing para tus clientes con Mailchimp"](https://www.spri.eus/euskadinnova/es/enpresa-digitala/agenda/como-hacer-mailing-para-clientes-con-mailchimp/5729.aspx).
- (2021) ["Organiza y gestiona tu correo de forma productiva: 5S Digitales, Inbox Zero, GTD. (Con Gmail)"](https://soka.gitlab.io/blog/post/2021-02-04-curso-inboxzero-gtdgmail-5sdigitales/) - Mikroenpresa Digitala.
- (2020) ["Licencias software libre. ¿Cómo impacta en mi desarrollo?"](https://www.spri.eus/euskadinnova/es/enpresa-digitala/agenda/licencias-software-libre-como-impacta-desarrollo-online/16671.aspx). Organizado por Tecnalia, impartido por Alejandra Ruiz ([@a_ruizTECNALIA](https://twitter.com/a_ruizTECNALIA)) ([ver contenidos en este blog](https://soka.gitlab.io/blog/post/2020-12-03-enpresadigitala_licencias-2020/)).
- (2020) Jornada ["Vigilancia de accesos remotos"](https://www.spri.eus/euskadinnova/es/enpresa-digitala/agenda/vigilancia-accesos-remotos-online/16471.aspx). Impartido por Sergio Alejo ([Talio](http://www.talio.it/)).
- (2020) Taller "Cómo hacer de tu hogar un espacio ciberseguro". Basque Cybersecurity Centre.
- (2020) Taller ["Aprende a producir vídeos con tu móvil para mejorar las ventas de tu proyecto"](https://www.spri.eus/euskadinnova/es/enpresa-digitala/agenda/aprende-producir-videos-con-movil-para-mejorar-ventas-proyecto-online/16389.aspx). [Filmatu](https://www.filmatu.com/).
- (2020) Jornada: ["Herramientas para teletrabajar de forma cómoda y efectiva"](https://www.spri.eus/euskadinnova/es/enpresa-digitala/agenda/herramientas-para-teletrabajar-forma-comoda-efectiva-online/16427.aspx). Impartido por Imanol Terán [https://www.itermar.io/](https://www.itermar.io/).
- (2019) Curso Angular (Ipartek).
- (2018) Fortificación y bastionado (hardening) en entornos Windows. Diego Gil (JakinCode)
- (2018) Taller SEO – Posicionamiento en buscadores. Fran Murillo (Mondragon Unibertsitatea)
- (2018) “Introducción a la programación en lenguaje R“. Germán Alonso (C2B)
- (2018) ERKIDE Ipartek “1551 – Gestión de proyectos con metodologías ágiles: SCRUM”.
- (2018) Jornada “Kanban Maturity Model. Gestión de proyectos y servicios en una organización fit-for-purpose” Teodora Bozheva (BerriProcess).
- (2018) Taller: Colaboración y productividad con soluciones O365 (Aitor Ezkerra Cano).
- (2018) Taller: Redes sociales: estrategia y monitorización en social media. Amaia García Dosouto (notepierdasenlasredes.com).
- (2018) Taller: Conseguir más ventas y clientes con la nueva Experiencia de Google AdWords. Impartido por: Pablo Jiménez Zamora (Otromarketing)
- (2017) Taller: Aprende a administrar la gestión diaria de tu tienda online en PrestaShop – Ana Lozano (Ilusianet)
- (2017) Taller: Child Themes en WordPress. Imanol Terán (WIDDIAPPS)
- (2017) Taller: Cómo hacer el trabajo fluir. Teodora Bozheva (BerriProcess).
- (2017) Jornada: Posibilidades de WordPress en 2017. Imanol Terán Maruri – @itermar
- (2017) Herramientas tecnológicas y venta online. ¿Cómo optimizar las ventas de tu empresa?. On4u expertos en Magento y estrategias de venta digital.
- (2017) Taller: Visualización de datos con Google Data Studio
- (2017) Taller: Analiza grandes volúmenes de datos con PowerPivot
- (2017) React Native: apps multiplataforma (curso)
- (2016-2017) Curso C# (Ceintec)
- (2015) Monta una tienda online con WooCommerce – SPRI – Miramon Enpresa Digitala (Daniel Arrilucea)
- (2015) Jornada: Visualizar el flujo de trabajo: 9+1 casos de éxito – impartida por @tbozheva de #Berriprocess
- (2015) Scrum y la Gestión ágil de proyectos – Nerea Molinero (Pragmatic)
- (2015) Cómo mejorar tu productividad en tu trabajo con GTD y herramientas digitales – Mondragon Unibertsitatea.
- (2015) Introducción a Scrum – Agilar.
- (2015) Visualiza datos gráficamente en la web con D3.js – Mondragon Unibertsitatea
- (2015) Mitos y realidades del Big Data – Mondragon Unibertsitatea – Varios
- (2014) Taller Business Model Canvas en entornos digitales – Mondragon Unibertsitatea
- (2014) Cómo gestionar información usando mapas mentales – xarsared.
- (2014) Gestión de proyectos con Kanban – Berriprocess.
- (2013) Google Sites para microempresas – KZgunea.
- (2013) Responsive design para desarrolladores – Sareblog S.L.
- (2013) Diseñar con Responsive Web Design – Sareblog S.L.
- (2013) Desarrollo de APPs en HTML5 (HTML5, CSS3, Javascript y jQuery) ‐ Eutokia.
- (2013) Curso Prezi, consigue resultados diferentes con presentaciones diferentes – Tecnalia
- (2013) Joomla 2.5: diseño paso a paso de un sitio web real – Tecnalia.
- (2008) Curso Java – Academia Consultec.
- (2006) Curso Iniciación Java – Academia Ceintec.
- (2005) Curso: XHTML y Maquetación CSS – Bizkaia Enpresa Digitala.
- (2005) Programación en lenguajes estructurados y de 4º generación (JavaScript, CSS, ASP, SQL Server 2002)- Centro San Luis.
- (2005) Jornada: Soluciones Web y de monitorización con Software libre (LAMP y Nagios) – Bizkaia Empresa Digitala
- (2005) Curso Intensivo: PHP y MySQL – Academia Ceintec.
- (2002-2003) Curso de Interconexión de redes Cisco – Universidad de Mondragón.
- (2000) Curso de Tecnología Oracle: Administración Oracle 8 y Developer 2000 V2.1 – Universidad de Mondragón.
