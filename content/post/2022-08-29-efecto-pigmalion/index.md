---
title: Efecto Pigmalión
subtitle: Influencia de la mayoría y la conformidad
date: 2022-08-29
draft: false
description: Influencia de la mayoría y la conformidad
tags: ["Equipos","Vídeos","Dinámicas","Educación","Experimentos","Conformidad","Influencia","Pigmalión","Personas","Educación"]
---

El efecto Pigmalión se puede identificar de las siguientes maneras:

* Suceso por el que una persona consigue lo que se proponía previamente a causa de la creencia de que puede conseguirlo.
* "Las expectativas y previsiones de los profesores sobre la forma en que de alguna manera se conducirán los alumnos determinan precisamente las conductas que los profesores esperan." (Rosenthal y Jacobson).
* Una profecía autocumplida es una expectativa que incita a las personas a actuar en formas que hacen que la expectativa se cumpla.

[https://es.wikipedia.org/wiki/Efecto_Pigmali%C3%B3n](https://es.wikipedia.org/wiki/Efecto_Pigmali%C3%B3n)


{{< youtube id="XwMWSUJKHYQ" >}}  



