---
title: El pase Invisible
subtitle: Test de atención
date: 2022-08-26
draft: false
description: Test de atención
tags: ["Atención","Vídeos","Dinámicas","Educación","Experimentos"]
---

{{< youtube id="PbVYH8FCLvo" >}}    

La atención es un recurso limitado de nuestra mente.
Sin embargo, la mayoría de las personas tienen la ilusion que no es así, y que todo lo que ocurre frente a sus ojos no pasará desapercibido a su atención.

El gorila invisible fue un experimento sencillo diseñado por dos profesores universitarios, publicado en Perception 1999, pag.1059, y realizado con estudiantes e psicología de la Universidad de Harvard.

Christopher Chabris es doctorado en Harvard y profesor de psicología del Union College de Nueva York.
Daniel Simons es doctorado en Cornell, profesor de psicología de la Universidad de Illinois y uno de los más importantes investigadores del mundo en cognición visual