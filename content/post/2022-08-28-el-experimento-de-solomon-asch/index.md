---
title: El experimento de Solomon Asch
subtitle: Influencia de la mayoría y la conformidad
date: 2022-08-26
draft: false
description: Influencia de la mayoría y la conformidad
tags: ["Equipos","Vídeos","Dinámicas","Educación","Experimentos","Conformidad","SolomonAsch","Influencia","Disonancia"]
---

{{< youtube id="wt9i7ZiMed8" >}}    


“Al mismo tiempo que Leon Festinger se inflitraba en la secta de la señora Martin, el psicólogo estadounidense Solomon Asch demostró que la presión del grupo puede llegar a ser la causa de que no hagamos caso a lo que vemos directamente con nuestros propios ojos. En un famoso experimento, mostró a los sujetos de su test tres líneas en una tarjeta y les preguntó cuál era la más larga. Cuando el resto de las personas presentes en la sala (todos colaboradores de Asch desconocidos por el sujeto) daban la misma respuesta, el sujeto también la daba, pese a que era claramente errónea.No es distinto en política. Los politólogos han establecido que el voto de la gente no está tan determinado por sus percepciones sobre sus propias vidas como por sus concepciones de la sociedad. No nos interesa tanto lo que el gobierno puede hacer por nosotros personalmente como saber qué puede hacer por todos nosotros. Cuando votamos, no lo hacemos pensando sólo en nosotros mismos, sino en el grupo al que deseamos pertenecer.”

“Solomon Asch hizo otro descubrimiento más. Una sola voz discordante puede cambiarlo todo. Cuando sólo otra persona en el grupo se ceñía a la verdad, los sujetos del test tenían más probabilidades de confiar en la información que les transmitían sus sentidos. Que esto sirva para reconfortar a todos aquellos que se sienten como una voz solitaria que clama en el desierto: seguid construyendo esos castillos en el aire. Vuestro momento llegará.
Larga”

Pasaje de "Utopía para realistas" Rutger Bregman

