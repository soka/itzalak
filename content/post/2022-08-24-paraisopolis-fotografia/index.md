---
title: Paraisópolis (foto de Tuca Vieira)
subtitle: La desigualdad social en una foto
date: 2022-08-24
draft: true
description: 
tags: ["Fotografía","Arquitectura","Sociedad","Brasil"]
---

![](img/01.jpg)

Fotografía tomada por Tuca Vieira (2004) en la favela Paraisópolis donde se ubica el barrio rico de Morumbi, como dice el propio autor, la fotografía refleja las profundas desigualdades entre ricos y pobres en Brasil, las personas que viven en la parte izquierda son las que trabajan para los moradores de las grandes mansiones lujosas con piscina de la derecha.


Entrevista realizada por BBC News Brasil a Tuca Vieira: [https://noticias.uol.com.br/ultimas-noticias/bbc/2019/12/04/triste-saber-que-ela-continuara-atual-diz-autor-de-foto-simbolo-de-paraisopolis-que-volta-a-viralizar-apos-mortes.htm](https://noticias.uol.com.br/ultimas-noticias/bbc/2019/12/04/triste-saber-que-ela-continuara-atual-diz-autor-de-foto-simbolo-de-paraisopolis-que-volta-a-viralizar-apos-mortes.htm)