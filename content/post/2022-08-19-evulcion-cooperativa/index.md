---
title: Evolución cooperativa
subtitle: Más Margulis, menos Darwin
date: 2022-08-19
draft: false
description: Más Margulis, menos Darwin 
tags: ["Evolución","Cooperación","Sociedad","Margulis","Lynn Margulis","Darwin","Yuval Noah Harari","Darwinismo social","STEAM","Mujeres","Ciencia","Apoyo mutuo","Solidaridad"]
---

La ciencia y tecnología de forma inevitable llevan imbricadas una visión del mundo en forma de los valores de sus creadores, a su vez algunas teorías cientificas permiten albergar esperanzas sobre visiones nuevas y alternativas, este es el caso de las teorías de la bióloga estadounidense [**Lynn Margulis**](https://es.wikipedia.org/wiki/Lynn_Margulis).

## Darwinismo social

La teoría evolucionista de [**Charles Darwin**](https://es.wikipedia.org/wiki/Charles_Darwin) (1809-1882) se concentra en su obra fundamental ["El origen de las especies"](https://es.wikipedia.org/wiki/El_origen_de_las_especies) (1859), la obra ve la luz en la Inglaterra, coincide en el tiempo con el auge de la [segunda revolución  industrial](https://es.wikipedia.org/wiki/Segunda_Revoluci%C3%B3n_Industrial) y la consolidación del modelo capitalista, sus ideas (principálmente el proceso denominado selección natural) han sido usadas para justificar estos sistemas ya que encajan muy bien con la idea de que la evolución social se apuntala sobre el éxito o supervivencia del más apto a través de una competencia feroz entre individuos, esto se conoce como [**Darwinismo social**](https://es.wikipedia.org/wiki/Darwinismo_social), se basa en la utilización de leyes naturales que han servido para justificar opciones morales o sociales como el [_laissez faire_](https://es.wikipedia.org/wiki/Laissez_faire) o la economía de [libre mercado](https://es.wikipedia.org/wiki/Mercado_libre) bajo la premisa de que este debería funcionar sin intervención y tienda a su propio equilibrio, e incluso la [eugenesia del nazismo](https://es.wikipedia.org/wiki/Eugenesia_nazi).

Problemas actuales que estamos viviendo como la crisis medioambiental son un ejemplo que nos pueden hacer preguntarnos si entendimos la evolución de forma parcial. [**Kropotkin**](https://es.wikipedia.org/wiki/Piotr_Kropotkin) considerado como uno de los principales teóricos del movimiento anarquista, en su obra ["El apoyo mutuo: un factor en la evolución"](https://es.wikipedia.org/wiki/El_apoyo_mutuo) (1902) desarrolló la teoría del [apoyo mutuo](https://es.wikipedia.org/wiki/Apoyo_mutuo) y la cooperación como un mecanismo de supervivencia del reino animal.

> «Pero la sociedad, en la humanidad, de ningún modo se ha creado sobre el amor ni tampoco sobre la simpatía. Se ha creado sobre la conciencia -aunque sea instintiva- de la solidaridad humana y de la dependencia recíproca de los hombres. Se ha creado sobre el reconocimiento inconsciente semiconsciente de la fuerza que la práctica común de dependencia estrecha de la felicidad de cada individuo de la felicidad de todos, y sobre los sentimientos de justicia o de equidad, que obligan al individuo a considerar los derechos de cada uno de los otros como iguales a sus propios derechos.» Piotr Kropotkin, Introducción a El apoyo mutuo.

## Lynn Margulis

Las herencia de las las teorías darwinistas han pesado mucho hasta nuestros días, [**Lynn Margulis**](https://es.wikipedia.org/wiki/Lynn_Margulis) y sus brillantes descubrimientos sin embargo tuvieron que luchar para que fuesen tomados en consideración en el mundo cientifico y académico. Margulis (1938-2011) fue una destacada bióloga estadounidense, sus teorías en el campo evolución biológica fueron disruptivas, la idea fundamental es que celulas complejas se originaron mediante la **simbiosis** de celulas más sencillas en una relación de beneficio mutuo, su original propuesta choca con la ortodoxia y prejuicios de la teoría darwinista, así Margulis desmostró el **valor que la cooperación en las fuerzas evolutivas**.

![](img/01.png)

> «El pacto es la simbiosis, al final nadie gana ni pierde sino que hay una recombinación. Se construye algo nuevo».

## ¿Cual es la clave del éxito de nuestra especie?

Seguramente si me hubiesen preguntado esto a mi hubiese respondido algo así como la capacidad de crear y emplear herramientas complejas, quiza influenciado por Margulis, **Yuval Noah Harari** (1976), historiador y escritor de gran relevancia en la actualidad, propone una idea similar a la cientifica pero a escala humana. 

![](img/02.jpg)

En su obra ["Sapiens. De animales a dioses: Una breve historia de la humanidad"](https://www.casadellibro.com/libro-sapiens-de-animales-a-dioses/9788499926223/2655217) defiende la original idea de que **el éxito de nuestra especie se fundamenta sobre su capacidad de cooperación a gran escala**, lo que logra principalmente compartiendo imaginarios colectivos como pueden ser el dinero (un trozo de papel al que atribuimos un valor), los imperios, las religiones o las marcas. Si una mañana nos despertasemos y nos negasemos a seguir creyendo en algunos de estos mitos estos desaparecerían. 

Si no te has leido ninguno de los libros de **Yuval Noah Harari**, cosa que te recomiendo, puedes ver la siguiente entrevista que le realizan donde explica de forma resumida algunas de sus ideas principales.

Cuando ya no esté: Yuval Noah Harari (Parte 1/2).

{{< youtube hxuKo_VdM9o >}}

Cuando ya no esté: Yuval Noah Harari (Parte 2/2).

{{< youtube ECwY77VI3QM >}}

## Enlaces externos

* [https://es.wikipedia.org/wiki/Charles_Darwin](https://es.wikipedia.org/wiki/Charles_Darwin).
* [https://es.wikipedia.org/wiki/El_origen_de_las_especies](https://es.wikipedia.org/wiki/El_origen_de_las_especies).
* [https://es.wikipedia.org/wiki/Darwinismo_social](https://es.wikipedia.org/wiki/Darwinismo_social).
* [https://es.wikipedia.org/wiki/Lynn_Margulis](https://es.wikipedia.org/wiki/Lynn_Margulis).
* [https://es.wikipedia.org/wiki/Teor%C3%ADa_de_juegos](https://es.wikipedia.org/wiki/Teor%C3%ADa_de_juegos).
* [https://www.nytimes.com/es/2021/07/18/espanol/opinion/pandemia-cooperacion.html](https://www.nytimes.com/es/2021/07/18/espanol/opinion/pandemia-cooperacion.html).

{{< youtube PUTp2wQhd70 >}}




