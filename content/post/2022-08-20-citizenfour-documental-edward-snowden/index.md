---
title: Citizenfour documental
subtitle: Edward Snowden
date: 2022-08-20
draft: false
description: Narra como Snowden filtro datos que demuestran la vigilancia masiva indiscriminada que realiza el gobierno de EEUU sobre sus ciudadanos y el mundo entero.
tags: ["Vídeos","Edward Snowden","Wikileaks","Vigilancia","EEUU","Tecnología","Vigilancia Permanente","Snowden"]
---

El documental narra la historia de Edward Snowden desde el momento que se refugia en Hong Kong y comienza a filtrar documentos clasificados de EEUU que demuestran la vigilancia masiva a la que somete a sus ciudadanos y al mundo entero.

{{< youtube id="amtbncIL0hU" >}}

[https://youtu.be/amtbncIL0hU](https://youtu.be/amtbncIL0hU)

Libro recomendado: "VIGILANCIA PERMANENTE" EDWARD SNOWDEN.

![](img/01.jpg)

«La lucha por el derecho a la intimidad es la nueva lucha por nuestra libertad» Edward Snowden

El mayor escándalo diplomático de la historia. Una vida al servicio de la verdad. Las memorias que agitarán la geopolítica mundial.

«Me llamo Edward Snowden. Antes trabajaba para el gobierno, pero ahora trabajo para el pueblo.»

En 2013, Edward Snowden, responsable de la mayor filtración de inteligencia en la historia, sacudió al mundo revelando que el gobierno estadounidense tenía la capacidad de leer cada correo electrónico, escuchar cada llamada y entrometerse en los rincones de la vida privada de todos y cada uno de los ciudadanos del mundo.

En Vigilancia permanente, Snowden desgrana por primera vez por qué lo hizo, cómo ayudó a construir un sistema de vigilancia masivo y la crisis de conciencia que le llevó a destaparlo todo y poner en jaque al sistema. Como resultado de aquello, se inició una caza y captura internacional que a día de hoy sigue abierta.

Un protagonista fascinante, ingenioso y con una mente prodigiosa convertido a lo largo de su vida en soldado, analista de inteligencia, agente de la CIA y, ya en el exilio, en activista por el derecho a la privacidad. Un libro que nos alerta sobre la deriva autoritaria de los Estados, que denuncia la colaboración entre el espionaje y las grandes multinacionales de la era digital y que destapa cómo nos vigilan y de qué manera se comercia con nuestra información personal. Porque como anuncia Snowden en su libro, «la lucha por el derecho a la intimidad es la nueva lucha por nuestra libertad».

#VigilanciaPermanente
